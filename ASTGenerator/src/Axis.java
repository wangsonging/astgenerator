import java.util.HashMap;
import java.util.Map;

public class Axis {
	A request = new A();
	public void bug1(){
		
		A a = request.getA();
		
		if(a.getId() == null){
			//System.out.println(a.getId());
			//a.dosomething(request);
			a.dosomething2(a.getId());
		}
	}
	
	public class A {
	String id;

	public A(String id) {
		super();
		this.id = id;
	}
	
	public A() {

	}
	
	public void dosomething(A a){
		if(this.id == null)
			this.id =a.id;
	}
	
	public void dosomething2(String a){
		
	}
	
	public A getA(){
		return this;
	}
	
	public String getId(){
		return id;
	}
}
}

