import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.*;
import org.eclipse.jdt.core.dom.InfixExpression.Operator;

public class Example {
	
	private static String fileToString(String path) throws IOException {
        
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);
    }
	
	private static ASTNode getNullcheckingObject(IfStatement ifStmt) {

		if(ifStmt.getExpression() instanceof InfixExpression){
			
			InfixExpression exp = (InfixExpression) ifStmt.getExpression();
			if(exp.getOperator() == Operator.EQUALS){
				
				if(exp.getRightOperand() instanceof NullLiteral){
					return exp.getLeftOperand();
				}
				
				if(exp.getLeftOperand() instanceof NullLiteral){
					return exp.getRightOperand();
				}
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) throws IOException, JavaModelException {
		// TODO Auto-generated method stub
		String file =System.getProperty("user.dir")+"\\src\\Axis.java";
		
		JavaASTParser jParser = new JavaASTParser(fileToString(file));		
				
		for(IfStatement ifs:jParser.getIfStatements()){
			ASTNode nullCheckingObject	= getNullcheckingObject(ifs);
			
			if(nullCheckingObject instanceof MethodInvocation){
				MethodInvocation mi = (MethodInvocation) nullCheckingObject;
				System.out.println("m: "+mi.toString());
				System.out.println("m: "+mi.getName());
				System.out.println("m: "+mi.getExpression());
			}
			
			System.out.println("start: "+ifs.getStartPosition());
			System.out.println("end: "+(int)(ifs.getLength()+ifs.getStartPosition()));
			
			System.out.println("==========");
		}
		
		for(VariableDeclarationFragment c: jParser.getVariableDeclarationFragments()){
			System.out.println(c.toString());
			System.out.println("c name: "+c.getName());
			System.out.println("c exp: "+c.getInitializer());
		}
 }
}	
